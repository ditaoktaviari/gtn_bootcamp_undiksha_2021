<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\Produksi\Produksi::class, 'index'])->name('produksiList');
Route::post('/datatable', [App\Http\Controllers\Produksi\Produksi::class, 'datatable'])->name('produksiDataTable');

Route::get('/create', [App\Http\Controllers\Produksi\Produksi::class, 'create'])->name('produksiCreate');
Route::post('/insert', [App\Http\Controllers\Produksi\Produksi::class, 'insert'])->name('produksiInsert');

Route::get('/edit/{id}', [App\Http\Controllers\Produksi\Produksi::class, 'edit'])->name('produksiEdit');
Route::post('/update/{id}', [App\Http\Controllers\Produksi\Produksi::class, 'update'])->name('produksiUpdate');

Route::delete('/delete/{id}', [App\Http\Controllers\Produksi\Produksi::class, 'delete'])->name('produksiDelete');

Route::get('/clear-cache', function (){
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is Crealed";
});