@extends('../componnents/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Daftar Produksi
                    </h3>
                </div>
                <a href="{{ route('produksiCreate') }}" class="btn btn-success">TAMBAH PRODUKSI</a>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet akses-list">
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <table class="akses-list table table-bordered datatable"
                                data-url="{{ route('produksiDataTable') }}"
                                data-column="{{ json_encode($datatable_column) }}">
                            <thead>
                                <tr>
                                    <th width="20">No</th>
                                    <th width="no-sort">Kode Produksi</th>
                                    <th width="no-sort">Lokasi</th>
                                    <th width="no-sort">Tanggal Mulai</th>
                                    <th width="no-sort">Tanggal Selesai</th>
                                    <th width="no-sort">Status</th>
                                    <th width="no-sort">Publish</th>
                                    <th width="no-sort">Menu</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection